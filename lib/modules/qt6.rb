# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018-2023 Harald Sitter <sitter@kde.org>

require 'pp'
require 'date'
require 'logger'
require 'time'

require_relative '../bugzillabot'
require_relative '../constants'
require_relative '../monkey'

module Runner
  class Qt6
    def self.run(logger:)
      last_change = (DateTime.now.new_offset(0) - 3/24.0).to_datetime.iso8601
      to_tag = []

      Bugzillabot::Bug.search(creation_time: last_change) do |bug|
        if bug.keywords.include?('qt6')
          logger.info "Bug #{bug.id} already qt6 -> ignoring"
          next
        end

        has_qt6 = bug.comments.any? do |comment|
          File.fnmatch('/libQt6*.so', comment.text) || comment.text.include?('Qt Version: 6.')
        end
        unless has_qt6
          logger.info "Bug #{bug.id} does not contain 'Qt Version: 6.' -> NOT tagging qt6"
          next
        end

        logger.info "Bug #{bug.id} contains 'Qt Version: 6.' -> tagging qt6"
        to_tag << bug
      end

      to_tag.each do |bug|
        bug.update(keywords: { add: %w[qt6] })
      end
    end
  end
end
