# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'

require 'modules/qt6'

class Qt6Test < Minitest::Test
  def test_not_qt6
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: JSON.generate({ bugs: { "283477" => { comments: [] } }, comments: {} }))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key')

    Runner::Qt6.run(logger: Logger.new($stdout))

    refute_requested(put_request)
  end

  def test_qt6
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/comment.json"))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key').with do |x|
      data = JSON.parse(x.body)
      assert_equal(data.keys, %w[keywords])
      assert_includes(data, 'keywords')
      assert_includes(data['keywords'], 'add')
      assert_includes(data['keywords']['add'], 'qt6')
      true
    end.to_return(status: 200, body: JSON.generate({}))

    Runner::Qt6.run(logger: Logger.new($stdout))

    assert_requested(put_request)
  end

end
