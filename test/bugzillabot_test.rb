# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'test_helper'

class BugzillabotTest < Minitest::Test
  def test_config
    assert(Bugzillabot.config)
    assert(Bugzillabot.config.is_a?(Bugzillabot::Config))
  end
end
